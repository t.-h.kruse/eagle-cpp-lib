#include "schematic.h"
#include <gate.h>
#include <iostream>
#include <regex>

namespace eagle {
void Schematic::parseLibraries(rapidxml::xml_document<> &xml) {
	using namespace rapidxml;
	xml_node<> *libs_node = xml.first_node("eagle")->first_node("drawing")->first_node("schematic")->first_node("libraries");

	for (xml_node<> *lib_node = libs_node->first_node(); lib_node; lib_node = lib_node->next_sibling()) {
		xml_attribute<> *lib_attr = lib_node->first_attribute("name");
		std::string		 lib_name = lib_attr ? lib_attr->value() : "";
		Library *		 lib	  = new Library(lib_name);
		m_libraries.push_back(lib);

		// parse packages
		xml_node<> *pkgs_node = lib_node->first_node("packages");
		for (xml_node<> *pkg_node = pkgs_node->first_node(); pkg_node; pkg_node = pkg_node->next_sibling()) {
			xml_attribute<> *pkg_attr = pkg_node->first_attribute("name");
			std::string		 pkg_name = pkg_attr ? pkg_attr->value() : "";
			Package *		 pkg	  = new Package(pkg_name);

			for (xml_node<> *smd_node = pkg_node->first_node("smd"); smd_node; smd_node = smd_node->next_sibling("smd")) {
				xml_attribute<> *pad_attr = smd_node->first_attribute("name");
				std::string		 pad_name = pad_attr ? pad_attr->value() : "";
				pkg->addPad(new Pad(pad_name));
			}
			for (xml_node<> *smd_node = pkg_node->first_node("pad"); smd_node; smd_node = smd_node->next_sibling("pad")) {
				xml_attribute<> *pad_attr = smd_node->first_attribute("name");
				std::string		 pad_name = pad_attr ? pad_attr->value() : "";
				pkg->addPad(new Pad(pad_name));
			}

			lib->addPackage(pkg);
		}

		// parse symbols
		xml_node<> *syms_node = lib_node->first_node("symbols");
		for (xml_node<> *sym_node = syms_node->first_node(); sym_node; sym_node = sym_node->next_sibling()) {
			xml_attribute<> *sym_attr = sym_node->first_attribute("name");
			std::string		 sym_name = sym_attr ? sym_attr->value() : "";
			Symbol *		 sym	  = new Symbol(sym_name);

			for (xml_node<> *pin_node = sym_node->first_node("pin"); pin_node; pin_node = pin_node->next_sibling("pin")) {
				xml_attribute<> *pin_name_attr = pin_node->first_attribute("name");
				xml_attribute<> *pin_dir_attr  = pin_node->first_attribute("direction");
				std::string		 pin_name	   = pin_name_attr ? pin_name_attr->value() : "";
				std::string		 pin_dir	   = pin_dir_attr ? pin_dir_attr->value() : "io";
				if (!pin_name.empty()) {
					if (pin_dir == "nc") {
						sym->addPin(new Pin(pin_name, eagle::Pin::Direction::NC));
					} else if (pin_dir == "in") {
						sym->addPin(new Pin(pin_name, eagle::Pin::Direction::IN));
					} else if (pin_dir == "out") {
						sym->addPin(new Pin(pin_name, eagle::Pin::Direction::OUT));
					} else if (pin_dir == "io") {
						sym->addPin(new Pin(pin_name, eagle::Pin::Direction::IO));
					} else if (pin_dir == "oc") {
						sym->addPin(new Pin(pin_name, eagle::Pin::Direction::OC));
					} else if (pin_dir == "hiz") {
						sym->addPin(new Pin(pin_name, eagle::Pin::Direction::HIZ));
					} else if (pin_dir == "pas") {
						sym->addPin(new Pin(pin_name, eagle::Pin::Direction::PAS));
					} else if (pin_dir == "pwr") {
						sym->addPin(new Pin(pin_name, eagle::Pin::Direction::PWR));
					} else if (pin_dir == "sup") {
						sym->addPin(new Pin(pin_name, eagle::Pin::Direction::SUP));
					}
				}
			}

			lib->addSymbol(sym);
		}

		// parse devicesets
		xml_node<> *devicesets_node = lib_node->first_node("devicesets");
		for (xml_node<> *deviceset_node = devicesets_node->first_node(); deviceset_node; deviceset_node = deviceset_node->next_sibling()) {
			xml_attribute<> *deviceset_attr	  = deviceset_node->first_attribute("name");
			xml_attribute<> *prefix_attr	  = deviceset_node->first_attribute("prefix");
			xml_attribute<> *uservalue_attr	  = deviceset_node->first_attribute("uservalue");
			xml_node<> *	 description_node = deviceset_node->first_node("description");

			std::string deviceset_name		   = deviceset_attr ? deviceset_attr->value() : "";
			std::string deviceset_prefix	   = prefix_attr ? prefix_attr->value() : "";
			bool		deviceset_hasUserValue = uservalue_attr ? true : false;
			std::string description			   = description_node ? description_node->value() : "";

			Deviceset *ds = new Deviceset(deviceset_name, deviceset_prefix, description, deviceset_hasUserValue);
			// parse gates
			xml_node<> *gates_node = deviceset_node->first_node("gates");
			for (xml_node<> *gate_node = gates_node->first_node(); gate_node; gate_node = gate_node->next_sibling()) {
				xml_attribute<> *gate_attr = gate_node->first_attribute("name");
				xml_attribute<> *sym_attr  = gate_node->first_attribute("symbol");
				xml_attribute<> *swap_attr = gate_node->first_attribute("swaplevel");
				std::string		 gate_name = gate_attr ? gate_attr->value() : "";
				std::string		 sym_name  = sym_attr ? sym_attr->value() : "";
				int				 swaplevel = swap_attr ? std::strtol(swap_attr->value(), nullptr, 10) : -1;
				if (const Symbol *sym = lib->findSymbol(sym_name)) {
					ds->addGate(new Gate(gate_name, sym, swaplevel));
				}
			}
			lib->addDeviceset(ds);

			// parse devices
			xml_node<> *devices_node = deviceset_node->first_node("devices");
			for (xml_node<> *device_node = devices_node->first_node(); device_node; device_node = device_node->next_sibling()) {
				xml_attribute<> *device_attr = device_node->first_attribute("name");
				std::string		 device_name = device_attr ? device_attr->value() : "";

				std::map<std::string, std::string> attributes;
				// parse attributes
				for (xml_node<> *attribute_node = device_node->first_node("technologies")->first_node("technology")->first_node("attribute"); attribute_node;
					 attribute_node				= attribute_node->next_sibling()) {
					xml_attribute<> *name_attr	   = attribute_node->first_attribute("name");
					xml_attribute<> *value_attr	   = attribute_node->first_attribute("value");
					attributes[name_attr->value()] = value_attr->value();
				}
				Device *		 dev		  = nullptr;
				xml_attribute<> *package_attr = device_node->first_attribute("package");
				if (package_attr) {
					// find package
					if (const Package *pkg = lib->findPackage(package_attr->value())) {
						dev = new Device(device_name, ds, pkg, lib);

						// setup connections
						xml_node<> *connects_node = device_node->first_node("connects");
						if (connects_node) {
							for (xml_node<> *connect_node = connects_node->first_node("connect"); connect_node; connect_node = connect_node->next_sibling()) {
								xml_attribute<> *gate_attr	= connect_node->first_attribute("gate");
								xml_attribute<> *pin_attr	= connect_node->first_attribute("pin");
								xml_attribute<> *pad_attr	= connect_node->first_attribute("pad");
								std::string		 gate_name	= gate_attr ? gate_attr->value() : "";
								std::string		 pin_name	= pin_attr ? pin_attr->value() : "";
								std::string		 pad_number = pad_attr ? pad_attr->value() : "";

								if (const Gate *g = ds->gate(gate_name)) {
									if (const Pin *pin = g->symbol()->pin(pin_name)) {
										if (const Pad *pad = pkg->pad(pad_number)) {
											ds->assign(pad, pin);
										}
									}
								}
							}
						}

					} else {
						std::cout << "Package: " << package_attr->value() << " not found" << std::endl;
					}
				} else {
					dev = new Device(device_name, ds, nullptr, lib);
				}
				dev->setAttributes(attributes);
				lib->addDevice(dev);
			}
		}
	}
}

void Schematic::parseParts(rapidxml::xml_document<> &xml) {
	using namespace rapidxml;
	xml_node<> *parts_node = xml.first_node("eagle")->first_node("drawing")->first_node("schematic")->first_node("parts");

	for (xml_node<> *part_node = parts_node->first_node(); part_node; part_node = part_node->next_sibling()) {
		xml_attribute<> *name_attr	 = part_node->first_attribute("name");
		xml_attribute<> *lib_attr	 = part_node->first_attribute("library");
		xml_attribute<> *devset_attr = part_node->first_attribute("deviceset");
		xml_attribute<> *device_attr = part_node->first_attribute("device");
		xml_attribute<> *value_attr	 = part_node->first_attribute("value");
		std::string		 part_name	 = name_attr ? name_attr->value() : "";
		std::string		 lib_name	 = lib_attr ? lib_attr->value() : "";
		std::string		 device_name = device_attr ? device_attr->value() : "";
		std::string		 devset_name = devset_attr ? devset_attr->value() : "";
		std::string		 part_value	 = value_attr ? value_attr->value() : "";
		if (const Device *dev = findDevice(Device::full_name(devset_name, device_name))) {
			Part *p = new Part(part_name, part_value, dev);
			m_parts.push_back(p);

			// parse uesr defined attributes
			for (xml_node<> *attr_node = part_node->first_node(); attr_node; attr_node = attr_node->next_sibling()) {
				xml_attribute<> *name_attr	= attr_node->first_attribute("name");
				xml_attribute<> *value_attr = attr_node->first_attribute("value");
				std::string		 attr_name	= name_attr ? name_attr->value() : "";
				std::string		 attr_value = value_attr ? value_attr->value() : "";
				if (!attr_value.empty()) p->setAttribute(attr_name, attr_value);
			}
		}
	}
}

const Device *Schematic::findDevice(const std::string &fullName) const {
	for (const Library *lib : m_libraries) {
		if (const Device *dev = lib->findDevice(fullName)) return dev;
	}
	return nullptr;
}

const Part *Schematic::findPart(const std::string &name) const { return findPart(name, m_parts); }

const Part *Schematic::findPart(const std::string &name, const std::vector<const Part *> &parts) {
	for (const Part *p : parts) {
		if (name == p->name()) return p;
	}
	return nullptr;
}

#include <iostream>

/**
 * @brief Parses the sheets for its parts
 * todo: Since Gates of parts are not yet supported, the sheet ref. for parts are defined by the last gate found
 * @param xml reference to the xml(schematic) document
 */
void Schematic::parseSheets(rapidxml::xml_document<> &xml) {
	using namespace rapidxml;
	xml_node<> *sheets_node = xml.first_node("eagle")->first_node("drawing")->first_node("schematic")->first_node("sheets");
	for (xml_node<> *sheet_node = sheets_node->first_node(); sheet_node; sheet_node = sheet_node->next_sibling()) {
		xml_node<> *desc_node = sheet_node->first_node("description");
		std::string desc	  = desc_node ? desc_node->value() : "";
		Sheet *		sheet	  = new Sheet(static_cast<unsigned int>(m_sheets.size()) + 1, desc);  // eagle sheets start at 1
		m_sheets.push_back(sheet);

		xml_node<> *instances_node = sheet_node->first_node("instances");
		for (xml_node<> *instance_node = instances_node->first_node(); instance_node; instance_node = instance_node->next_sibling()) {
			xml_attribute<> *part_attr = instance_node->first_attribute("part");
			xml_attribute<> *gate_attr = instance_node->first_attribute("gate");
			std::string		 part_name = part_attr ? part_attr->value() : "";
			std::string		 gate_name = gate_attr ? gate_attr->value() : "";

			if (const Part *part = findPart(part_name)) {
				if (const Gate *gate = part->device()->deviceset()->gate(gate_name)) {
					sheet->addInstance(part, gate);
				} else
					std::cout << __FUNCTION__ << " gate not found: " << gate_name << std::endl;
			} else
				std::cout << __FUNCTION__ << " part not found: " << part_name << std::endl;
		}

		// parse nets
		for (xml_node<> *net_node = sheet_node->first_node("nets")->first_node(); net_node; net_node = net_node->next_sibling()) {
			xml_attribute<> *net_name_attr = net_node->first_attribute("name");
			std::string		 net_name	   = net_name_attr ? net_name_attr->value() : "";
			
			for(xml_node<> * segment_node = net_node->first_node("segment"); segment_node; segment_node = segment_node->next_sibling("segment")){
				for (xml_node<> *pinref_node = segment_node->first_node("pinref"); pinref_node;
					 pinref_node			 = pinref_node->next_sibling("pinref")) {
					xml_attribute<> *part_name_attr = pinref_node->first_attribute("part");
					xml_attribute<> *gate_name_attr = pinref_node->first_attribute("gate");
					xml_attribute<> *pin_name_attr	= pinref_node->first_attribute("pin");
					std::string		 part_name		= part_name_attr ? part_name_attr->value() : "";
					std::string		 gate_name		= gate_name_attr ? gate_name_attr->value() : "";
					std::string		 pin_name		= pin_name_attr ? pin_name_attr->value() : "";

					if (const Part *part = findPart(part_name)) {
						if (const Gate *gate = part->device()->deviceset()->gate(gate_name)) {
							if (const Pin *pin = gate->symbol()->pin(pin_name)) {
								Net *		  n = new Net(net_name);
								NetConnection c = { part, gate, pin };
								n->addConnection(c);
								sheet->addNet(n);
							}
						}
					}
				}
			}
		}
	}
}

void Schematic::uniqueAttributes(std::vector<std::string> &attributes, bool sorted) const {
	for (const Part *p : m_parts) {
		std::map<std::string, std::string> attrs;
		p->attributes(attrs);
		std::map<std::string, std::string>::const_iterator it;
		for (it = attrs.begin(); it != attrs.end(); it++) {
			if (std::find(attributes.begin(), attributes.end(), it->first) == attributes.end()) attributes.push_back(it->first);
		}
	}
	if(sorted) std::sort(attributes.begin(), attributes.end());
}

void Schematic::uniqueAttributes(std::vector<std::string> &attributes, const std::string &regex, bool sorted) const {
	std::vector<std::string> allAttributes;
	uniqueAttributes(allAttributes, sorted);
	std::regex re(regex);
	for (const std::string &attr : allAttributes) {
		if (std::regex_search(attr, re)) {
			attributes.push_back(attr);
		}
	}
}

void Schematic::groupPartsByValue(const std::vector<const Part *> &srcParts, std::map<const Part *, std::vector<const Part *>> &groupedParts,
								  std::vector<const Part *> &keys, bool sort) {
	for (const Part *p : srcParts) {
		std::vector<const Part *>::const_iterator it = p->findIn(keys);
		if (it != keys.end()) {
			groupedParts[*it].push_back(p);
		} else {
			keys.push_back(p);
			groupedParts[p] = { p };
		}
	}
	if (sort) {
		for (auto &entry : groupedParts) {
			std::sort(entry.second.begin(), entry.second.end(), [](const Part *lhs, const Part *rhs) -> bool {
				return lhs->lessThan(*rhs, Part::Sort::ByName);
			});
		}
	}
}

void Schematic::parseBoardLocations(rapidxml::xml_document<> &xml) {
	using namespace rapidxml;
	xml_node<> *elements_node = xml.first_node("eagle")->first_node("drawing")->first_node("board")->first_node("elements");
	for (xml_node<> *element_node = elements_node->first_node(); element_node; element_node = element_node->next_sibling()) {
		xml_attribute<> *name_attr = element_node->first_attribute("name");
		xml_attribute<> *x_attr	   = element_node->first_attribute("x");
		xml_attribute<> *y_attr	   = element_node->first_attribute("y");
		xml_attribute<> *rot_attr  = element_node->first_attribute("rot");	// optional

		std::string part_name = name_attr ? name_attr->value() : "";
		double		x		  = x_attr ? std::strtod(x_attr->value(), nullptr) : 0;
		double		y		  = y_attr ? std::strtod(y_attr->value(), nullptr) : 0;
		std::string rots	  = rot_attr ? rot_attr->value() : "";
		double		rot		  = 0;
		bool		bottom	  = false;

		if (!rots.empty()) {
			int offset = 1;
			if (rots[0] == 'M') {
				bottom = true;
				offset = 2;
			}
			rot = std::strtod(rots.c_str() + offset, nullptr);
		}

		if (Part *p = const_cast<Part *>(findPart(part_name))) {
			p->setBoardLocation(x, y, rot, bottom);
		}
	}
}

void Schematic::connectedNetsForPart(const Part *part, std::vector<const Net *> &connectedNets) const {
	for (const Sheet *s : m_sheets) {
		for (const Net *n : s->nets()) {
			if (n->connects(part)) connectedNets.push_back(n);
		}
	}
}

void Schematic::partsWithPinCount(int pincount, std::vector<const eagle::Part *> &parts) const {
	for (const Part *p : m_parts) {
		std::size_t pc = 0;
		for (const Gate *g : p->device()->deviceset()->gates()) {
			pc += g->symbol()->pinCount();
		}
		if (pc >= pincount) parts.push_back(p);
	}
}

const Sheet *Schematic::sheetForPart(const Part *part, const Gate *gate) const {
	if (gate)
		for (const Sheet *s : m_sheets) {
			if (s->hasGate(part, gate)) return s;
		}
	else
		for (const Sheet *s : m_sheets) {
			if (s->hasPart(part)) return s;
		}
	return nullptr;
}
}  // namespace eagle
