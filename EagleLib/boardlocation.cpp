#include "boardlocation.h"

namespace eagle {
BoardLocation::BoardLocation() : m_x(0), m_y(0), m_rotation(0), m_bottom(false) {}
BoardLocation::BoardLocation(double x, double y, double rotation, bool bottom) : m_x(x), m_y(y), m_rotation(rotation), m_bottom(bottom) {}

double BoardLocation::x() const { return m_x; }
double BoardLocation::y() const { return m_y; }
double BoardLocation::rotation() const { return m_rotation; }
bool   BoardLocation::bottom() const { return m_bottom; }
void   BoardLocation::setX(double newValue) { m_x = newValue; }
void   BoardLocation::setY(double newValue) { m_y = newValue; }
void   BoardLocation::setRotation(double newValue) { m_rotation = newValue; }
void   BoardLocation::setBottom(bool newValue) { m_bottom = newValue; }

}  // namespace eagle
