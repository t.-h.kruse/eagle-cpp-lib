#ifndef EAGLE_SYMBOL_H_
#define EAGLE_SYMBOL_H_

#include <ostream>
#include <string>
#include <vector>
#include "pin.h"

namespace eagle {
class Symbol {
public:
	Symbol(const std::string &name);
	~Symbol();

	std::string					   name() const;
	const std::vector<const Pin *> pins() const;
	const Pin *					   pin(const std::string &pinName) const;
	std::size_t					   pinCount() const;

	void addPin(const Pin *p);

private:
	std::string				 m_name;
	std::vector<const Pin *> m_pins;
};

std::ostream &operator<<(std::ostream &out, const Symbol &sym);
}  // namespace eagle

#endif	// EAGLE_SYMBOL_H_