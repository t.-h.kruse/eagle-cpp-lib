#include "package.h"

namespace eagle {
std::ostream &operator<<(std::ostream &out, const Package &pkg) {
	out << pkg.name();
	return out;
}

Package::~Package() {
	for (const Pad *p : m_pads) {
		delete p;
	}
}

Package::Package(std::string name) : m_name(name) {}

std::string Package::name() const { return m_name; }

const std::vector<const Pad *> Package::pads() const { return m_pads; }
std::size_t					   Package::padCount() const { return m_pads.size(); }

void	   Package::addPad(const Pad *pad) { m_pads.push_back(pad); }
const Pad *Package::pad(const std::string& name) const {
	for (const Pad *p : m_pads) {
		if (p->name() ==  name) return p;
	}
	return nullptr;
}
}  // namespace eagle