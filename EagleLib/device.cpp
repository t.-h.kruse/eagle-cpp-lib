#include "device.h"
#include <regex>

#include "library.h"

namespace eagle {
std::ostream &operator<<(std::ostream &out, const Device &device) {
	out << device.name() << "@" << device.library()->name();

	if (device.package() || !device.attributes().empty()) {
		out << "(";

		if (device.package()) out << "package: " << device.package()->name();

		std::map<std::string, std::string> attributes = device.attributes();
		if (!attributes.empty()) {
			out << ", attributes: [";

			auto										 secondLast = std::prev(attributes.end(), 1);
			std::map<std::string, std::string>::iterator it;
			for (it = attributes.begin(); it != secondLast; it++) {
				out << it->first << ": " << it->second << ", ";
			}

			out << it->first << ": " << it->second << "]";
		}
		out << ")";
	}
	return out;
}

std::string Device::full_name(const std::string &deviceset_name, const std::string &device_name) {
	std::string str(deviceset_name);
	std::regex	re("\\*\\?|\\?\\*|\\*|\\?", std::regex_constants::icase);
	if (std::regex_search(str, re)) {
		return std::regex_replace(str, re, device_name);
	} else {
		return str + device_name;
	}
}

Device::Device() : m_name(""), m_deviceset(nullptr), m_package(nullptr), m_library(nullptr) {}
Device::Device(const std::string& name) : m_name(name), m_deviceset(nullptr), m_package(nullptr), m_library(nullptr) {}
Device::Device(const std::string& name, const Deviceset *deviceset, const Package *package, const Library *library)
	: m_name(name), m_deviceset(deviceset), m_package(package), m_library(library) {}

std::string		 Device::name() const { return Device::full_name(m_deviceset->name(), m_name); }
const Deviceset *Device::deviceset() const { return m_deviceset; }
const Package *	 Device::package() const { return m_package; }
// const Symbol *	 Device::symbol() const {return m_symbol;}
const Library *Device::library() const { return m_library; }

std::string Device::attribute(const std::string &key) const {
	try {
		return m_attributes.at(key);
	} catch (const std::out_of_range &) {
		throw;
	}
}
const std::map<std::string, std::string> &Device::attributes() const { return m_attributes; }

bool Device::hasAttribute(const std::string &key) const { return (this->m_attributes.find(key) != this->m_attributes.end()); }

void Device::addAttribute(std::string key, std::string value) { this->m_attributes[key] = value; }
void Device::setAttributes(const std::map<std::string, std::string> &attributes) { this->m_attributes = attributes; }
}  // namespace eagle