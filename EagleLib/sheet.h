#ifndef EAGLE_SHEET_H_
#define EAGLE_SHEET_H_

#include <ostream>
#include <string>
#include <vector>

#include "net.h"
#include "partinstance.h"

namespace eagle {
class Sheet {
public:
	Sheet(unsigned int number, const std::string &desc = "");
	~Sheet();

	unsigned int				   number() const;
	std::string					   description() const;
	void						   setDescription(const std::string &newValue);
	const std::vector<const Net *> nets() const;
	void						   nets(const Part *part, const Gate *gate, std::vector<const Net *> &nets) const;
	void						   addNet(const Net *net);
	void						   addInstance(const Part *part, const Gate *gate);
	void						   addInstance(const PartInstance &instance);
	bool						   hasPart(const Part *part) const;
	bool						   hasGate(const Part *part, const Gate *gate) const;

private:
	unsigned int			  m_number;
	std::string				  m_description;
	std::vector<PartInstance> m_instances;
	std::vector<const Net *>  m_nets;
};

std::ostream &operator<<(std::ostream &out, const Sheet &sheet);
}  // namespace eagle

#endif	// EAGLE_SHEET_H_