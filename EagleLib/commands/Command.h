#ifndef EAGLE_COMMAND_H_
#define EAGLE_COMMAND_H_

#include <vector>
#include "../part.h"

namespace eagle {

class Command {
public:
	enum class CMD { AddAttribute, ChangeValue };

private:
	const CMD m_cmd;

protected:
	std::vector<const Part*> m_receivers;

	Command(const std::vector<const Part*>& parts, const CMD cmd) : m_receivers(parts), m_cmd(cmd) {}

public:
	virtual ~Command() {};
	virtual void exec() = 0;
	virtual void undo() = 0;

	CMD						  type() const { return m_cmd; }
	const std::vector<const Part*>& parts() const { return m_receivers; }
};

}  // namespace eagle

#include "AddAttributeCommand.h"
#include "ChangeValueCommand.h"

#endif	// EAGLE_COMMAND_H_