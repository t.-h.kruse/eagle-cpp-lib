#include "ChangeValueCommand.h"
namespace eagle {

ChangeValueCommand::ChangeValueCommand(const std::vector<const Part *> &parts, const std::string &newValue)
	: Command(parts, Command::CMD::ChangeValue), m_value(newValue), m_oldValue(parts.front()->value(false)) {}
void ChangeValueCommand::exec() {
	for (const Part *p : Command::m_receivers) {
		const_cast<Part *>(p)->setValue(m_value);
	}
}
void ChangeValueCommand::undo() {
	for (const Part *p : Command::m_receivers) {
		const_cast<Part *>(p)->setValue(m_oldValue);
	}
}

}  // namespace eagle
