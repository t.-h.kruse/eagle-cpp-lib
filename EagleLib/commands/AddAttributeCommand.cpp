#include "AddAttributeCommand.h"

namespace eagle {

AddAttributeCommand::AddAttributeCommand(const std::vector<const Part*>& parts, const std::string& key, const std::string& newValue)
	: Command(parts, Command::CMD::AddAttribute), m_key(key), m_value(newValue), m_oldValues(), m_newKey(true) {
	for (const Part* p : parts) {
		if (p->hasAttribute(m_key, false)) {
			m_oldValues.push_back({ p->attribute(m_key, false), false });
		} else {
			m_oldValues.push_back({ "", true });
		}
	}
}
void AddAttributeCommand::exec() {
	for (const Part* p : Command::m_receivers) {
		const_cast<Part*>(p)->setAttribute(m_key, m_value);
	}
}

void AddAttributeCommand::undo() {
	for (std::size_t i = 0; i < Command::m_receivers.size(); i++) {
		Part*								p		 = const_cast<Part*>(Command::m_receivers[i]);
		const std::pair<std::string, bool>& pair	 = m_oldValues[i];
		const std::string&					oldValue = pair.first;
		const bool							newKey	 = pair.second;
		if (newKey)
			p->deleteAttribute(m_key);
		else
			p->setAttribute(m_key, oldValue);
	}
}

}  // namespace eagle