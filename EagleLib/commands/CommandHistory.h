#ifndef EAGLE_COMMANDHISTORY_H_
#define EAGLE_COMMANDHISTORY_H_

#include <list>
#include "Command.h"

namespace eagle {
class CommandHistory {
private:
	std::list<Command*>			  m_commands;
	std::list<Command*>::iterator m_lastCommand;
	unsigned int				  m_size;
	unsigned int				  m_maxSize;

public:
	CommandHistory(unsigned int historySize = 100);
	~CommandHistory();

	/**
	 * @brief Pushes and executes a new command
	 * If the history limit is reached the history cuts the oldest command.
	 * This class takes ownership of the command
	 *
	 * @param cmd
	 */
	void push(Command* cmd);

	/**
	 * @brief Resets/Clears the command buffer
	 * 
	 */
	void flush();

	/**
	 * @brief Undo the last action
	 * 
	 * @return const Command* Command that was executed else nullptr
	 */
	const Command* undo();

	/**
	 * @brief Redo the last action
	 * 
	 * @return const Command* Command that was executed else nullptr
	 */
	const Command* redo();
};

}  // namespace eagle

#endif	// EAGLE_COMMANDHISTORY_H_