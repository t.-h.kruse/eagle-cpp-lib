#ifndef EAGLE_CHANGEVALUECOMMAND_H_
#define EAGLE_CHANGEVALUECOMMAND_H_

#include <string>
#include "Command.h"

namespace eagle {
class ChangeValueCommand : public Command {
private:
	const std::string m_value;
	const std::string m_oldValue;

public:
	ChangeValueCommand(const std::vector<const Part*>& parts, const std::string& newValue);
	void exec();
	void undo();
};
}  // namespace eagle

#endif	// EAGLE_CHANGEVALUECOMMAND_H_