

#include "CommandHistory.h"

namespace eagle {

CommandHistory::CommandHistory(unsigned int historySize) : m_commands(), m_size(0), m_maxSize(historySize), m_lastCommand(m_commands.begin()) {}
CommandHistory::~CommandHistory() {
	for (Command* c : m_commands) {
		delete c;
	}
}

void CommandHistory::push(Command* cmd) {
	// when the user pushes a new command, every command after m_lastCommand is removed
	if (m_lastCommand != m_commands.end()) {
		for (auto it = m_lastCommand; it != m_commands.end(); it++) {
			delete *it;
		}
		m_commands.erase(m_lastCommand, m_commands.end());
		m_size = static_cast<unsigned int>(m_commands.size());
	}

	// remove oldest command if command list is full
	if (m_size >= m_maxSize) {
		delete m_commands.front();
		m_commands.pop_front();
	} else {
		m_size++;
	}
	m_commands.push_back(cmd);
	cmd->exec();
	m_lastCommand = m_commands.end();
}

void CommandHistory::flush() {
	// delete every command on the stack
	for (auto it = m_commands.begin(); it != m_commands.end(); it++) {
		delete *it;
	}
	// erase elements from container
	m_commands.erase(m_commands.begin(), m_commands.end());
	m_size		  = 0;
	m_lastCommand = m_commands.begin();
}

const Command* CommandHistory::undo() {
	if (m_lastCommand != m_commands.begin()) {
		m_lastCommand = std::prev(m_lastCommand);
		Command* cmd  = *m_lastCommand;
		cmd->undo();
		return cmd;
	}
	return nullptr;
}

const Command* CommandHistory::redo() {
	if (m_lastCommand != m_commands.end()) {
		Command* cmd = *m_lastCommand;
		cmd->exec();
		m_lastCommand++;
		return cmd;
	}
	return nullptr;
}

}  // namespace eagle
