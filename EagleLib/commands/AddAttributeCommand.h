#ifndef EAGLE_ADDATTRIBUTECOMMAND_H_
#define EAGLE_ADDATTRIBUTECOMMAND_H_
#include <string>
#include "Command.h"

namespace eagle {
class AddAttributeCommand : public Command {
private:
	const std::string						  m_key, m_value;
	std::vector<std::pair<std::string, bool>> m_oldValues;
	bool									  m_newKey;

public:
	AddAttributeCommand(const std::vector<const Part*>& parts, const std::string& key, const std::string& newValue);
	void exec();
	void undo();
};
}  // namespace eagle

#endif	// EAGLE_ADDATTRIBUTECOMMAND_H_