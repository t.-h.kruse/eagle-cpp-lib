#include "symbol.h"

namespace eagle {
std::ostream &operator<<(std::ostream &out, const Symbol &sym) {
	out << sym.name();
	return out;
}

Symbol::Symbol(const std::string &name) : m_name(name) {}
Symbol::~Symbol() {
	for (const Pin *p : m_pins) {
		delete p;
	}
}

std::string Symbol::name() const { return m_name; }

const std::vector<const Pin *> Symbol::pins() const { return m_pins; }

const Pin *Symbol::pin(const std::string &pinName) const {
	for (const Pin *p : m_pins) {
		if (p->name() == pinName) return p;
	}
	return nullptr;
}

std::size_t Symbol::pinCount() const { return m_pins.size(); }

void Symbol::addPin(const Pin *p) { m_pins.push_back(p); }
}  // namespace eagle