#include "net.h"

namespace eagle {
Net::Net(const std::string& name) : m_name(name) {}

std::string Net::name() const { return m_name; }

void Net::addConnection(const NetConnection& con) { m_connectedParts.push_back(con); }

bool Net::connects(const Part* p) const {
	for (const NetConnection& con : m_connectedParts) {
		if (con.part == p) return true;
	}
	return false;
}
const Pin* Net::connects(const Part* p, const Gate* g) const {
	for (const NetConnection& con : m_connectedParts) {
		if (con.part == p && con.gate == g) return con.pin;
	}
	return nullptr;
}
bool Net::connects(const Part* part, const Gate* gate, const Pin* pin) const {
	for (const NetConnection& con : m_connectedParts) {
		if (con.part == part && con.gate == gate && con.pin == pin) return true;
	}
	return false;
}
}  // namespace eagle