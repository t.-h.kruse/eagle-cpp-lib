#ifndef EAGLE_DEVICE_H_
#define EAGLE_DEVICE_H_

#include <map>
#include <ostream>
#include <string>

#include "deviceset.h"
#include "package.h"

namespace eagle {
class Library;
/**
 * @brief A Device links the Package with the Deviceset
 *
 */
class Device {
	std::string						   m_name;		/**< Device-specifier like "BRUZ" for the AD5791*/
	const Package *					   m_package;	/**< Reference to the Package (for convenience)*/
	const Deviceset *				   m_deviceset; /**< Reference to the Deviceset*/
	const Library *					   m_library;	/**< Reference to the Library this Device is in*/
	std::map<std::string, std::string> m_attributes;

public:
	Device();
	Device(const std::string& name);
	Device(const std::string& name, const Deviceset *deviceset, const Package *package, const Library *library);

	/**
	 * @brief Gets the full device name
	 * The Full device name consits of the Deviceset name plus the Device name
	 * EAGLE's special characters "*?" define where the Device name will be
	 * placed. Without the special chars it will be appended.
	 * @return std::string Returns the full device name
	 */
	std::string	   name() const;
	const Package *package() const;
	// const Symbol *	 symbol() const;
	const Deviceset *deviceset() const;
	const Library *	 library() const;
	std::string		 attribute(const std::string &key) const;

	const std::map<std::string, std::string> &attributes() const;

	bool hasAttribute(const std::string &key) const;
	void addAttribute(std::string key, std::string value);
	void setAttributes(const std::map<std::string, std::string> &attributes);

	static std::string full_name(const std::string &deviceset_name, const std::string &device_name);
};

std::ostream &operator<<(std::ostream &out, const Device &device);
}  // namespace eagle
#endif	// EAGLE_DEVICE_H_
