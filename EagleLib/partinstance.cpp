#include "partinstance.h"


namespace eagle {
PartInstance::PartInstance(const Part *part, const Gate *gate) : m_part(part), m_gate(gate) {}

const Part *PartInstance::part() const { return m_part; }
const Gate *PartInstance::gate() const { return m_gate; }
}  // namespace eagle