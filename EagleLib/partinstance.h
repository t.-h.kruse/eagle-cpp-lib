#ifndef EAGLE_PARTINSTANCE_H_
#define EAGLE_PARTINSTANCE_H_

namespace eagle {
class Part;
class Gate;

class PartInstance {
public:
	PartInstance(const Part *part, const Gate *gate);

	const Part *part() const;
	const Gate *gate() const;

private:
	const Part *m_part;
	const Gate *m_gate;
};
}  // namespace eagle

#endif	// EAGLE_PARTINSTANCE_H_