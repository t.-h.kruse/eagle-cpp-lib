#include "library.h"

namespace eagle {
std::ostream &operator<<(std::ostream &out, const Library &lib) {
	for (const Deviceset *devset : lib.devicesets()) {
		std::list<const Device *> devicesOfDeviceset;
		lib.devicesOf(devset, devicesOfDeviceset);

		for (const Device *dev : devicesOfDeviceset) {
			out << *dev << std::endl;
		}
	}
	return out;
}

Library::Library(std::string name) : m_name(name), m_packages(), m_symbols(), m_devices(), m_devicesets() {}

Library::~Library() {
	for (const Package *p : m_packages) {
		delete p;
	}
	for (const Symbol *s : m_symbols) {
		delete s;
	}
	for (const Device *d : m_devices) {
		delete d;
	}
	for (const Deviceset *ds : m_devicesets) {
		delete ds;
	}
}

std::string							Library::name() const { return m_name; }
const std::list<const Package *> &	Library::packages() const { return m_packages; }
const std::list<const Symbol *> &	Library::symbols() const { return m_symbols; }
const std::list<const Device *> &	Library::devices() const { return m_devices; }
const std::list<const Deviceset *> &Library::devicesets() const { return m_devicesets; }
void								Library::devicesOf(const Deviceset *set, std::list<const Device *> &outDevices) const {
	   for (const Device *dev : m_devices) {
		   if (dev->deviceset()->name() == set->name()) {
			   outDevices.push_back(dev);
		   }
	   }
}

void Library::addPackage(const Package *pkg) { m_packages.push_back(pkg); }
void Library::addSymbol(const Symbol *sym) { m_symbols.push_back(sym); }
void Library::addDevice(const Device *dev) { m_devices.push_back(dev); }
void Library::addDeviceset(const Deviceset *set) { m_devicesets.push_back(set); }

const Package *Library::findPackage(const std::string &name) const {
	for (const Package *pkg : m_packages) {
		if (pkg->name() == name) return pkg;
	}
	return nullptr;
}

const Symbol *Library::findSymbol(const std::string &name) const {
	for (const Symbol *sym : m_symbols) {
		if (sym->name() == name) return sym;
	}
	return nullptr;
}

const Device *Library::findDevice(const std::string &fullName) const {
	for (const Device *dev : m_devices) {
		if (dev->name() == fullName) return dev;
	}
	return nullptr;
}
}  // namespace eagle