cmake_minimum_required(VERSION 3.10)

set(TARGET EagleLib)

project(${TARGET} LANGUAGES CXX)

include(GNUInstallDirs)

set(${TARGET}_VERSION_MAJOR 0)
set(${TARGET}_VERSION_MINOR 1)
set(${TARGET}_VERSION_PATCH 0)

set(${TARGET}_VERSION
    ${EagleLib_VERSION_MAJOR}.${EagleLib_VERSION_MINOR}.${EagleLib_VERSION_PATCH}
)

add_library(
  ${TARGET}
  boardlocation.cpp
  schematic.cpp
  sheet.cpp
  device.cpp
  library.cpp
  deviceset.cpp
  package.cpp
  gate.cpp
  partinstance.cpp
  net.cpp
  symbol.cpp
  pin.cpp
  pad.cpp
  part.cpp
  commands/ChangeValueCommand.cpp
  commands/AddAttributeCommand.cpp
  commands/CommandHistory.cpp)

# Headers to be installed
list(
  APPEND
  ${TARGET}_Headers
  boardlocation.h
  schematic.h
  sheet.h
  device.h
  library.h
  deviceset.h
  package.h
  symbol.h
  net.h
  gate.h
  partinstance.h
  part.h
  pin.h
  pad.h)
list(APPEND ${TARGET}_RAPIDXML_Headers rapidxml/rapidxml_iterators.hpp
     rapidxml/rapidxml_print.hpp rapidxml/rapidxml_utils.hpp
     rapidxml/rapidxml.hpp)
list(APPEND ${TARGET}_Commands_Headers commands/Command.h
     commands/CommandHistory.h commands/ChangeValueCommand.h
     commands/AddAttributeCommand.h)

target_include_directories(
  ${TARGET}
  PUBLIC $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
         $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
         $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>)

# installation
include(GenerateExportHeader)
generate_export_header(EagleLib)
set_target_properties(EagleLib PROPERTIES VERSION ${EagleLib_VERSION})
set_target_properties(EagleLib PROPERTIES INTERFACE_EagleLib_MAJOR_VERSION
                                          ${EagleLib_VERSION_MAJOR})
set_target_properties(EagleLib PROPERTIES COMPATIBLE_INTERFACE_STRING
                                          EagleLib_VERSION_MAJOR)

install(
  TARGETS EagleLib
  EXPORT EagleLibTargets
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}/EagleLib/${CMAKE_BUILD_TYPE})
install(FILES ${EagleLib_Headers}
        DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}/EagleLib")
install(FILES ${EagleLib_RAPIDXML_Headers}
        DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}/EagleLib/rapidxml")
install(FILES ${EagleLib_Commands_Headers}
        DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}/EagleLib/commands")

include(CMakePackageConfigHelpers)
write_basic_package_version_file(
  "${CMAKE_CURRENT_BINARY_DIR}/EagleLib/EagleLibConfigVersion.cmake"
  VERSION ${EagleLib_VERSION}
  COMPATIBILITY AnyNewerVersion)

export(
  EXPORT EagleLibTargets
  FILE "${CMAKE_CURRENT_BINARY_DIR}/EagleLib/EagleLibTargets.cmake"
  NAMESPACE EagleLib::)

configure_file(
  cmake/EagleLibConfig.cmake
  "${CMAKE_CURRENT_BINARY_DIR}/EagleLib/EagleLibConfig.cmake" COPYONLY)

set(ConfigPackageLocation lib/cmake/EagleLib)
install(
  EXPORT EagleLibTargets
  FILE EagleLibTargets.cmake
  NAMESPACE EagleLib::
  DESTINATION ${ConfigPackageLocation})

install(FILES cmake/EagleLibConfig.cmake
              "${CMAKE_CURRENT_BINARY_DIR}/EagleLib/EagleLibConfigVersion.cmake"
        DESTINATION ${ConfigPackageLocation})
