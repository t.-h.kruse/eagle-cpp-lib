#ifndef EAGLE_SCHEMATIC_H_
#define EAGLE_SCHEMATIC_H_

#include <fstream>
#include <iterator>
#include <string>
#include <vector>

#include "rapidxml/rapidxml.hpp"
#include "rapidxml/rapidxml_utils.hpp"

#include "library.h"
#include "part.h"
#include "sheet.h"

// template<class T> struct ptr_less {
// 	bool operator()(const T*lhs, const T*rhs) const {
// 		return *lhs < *rhs;
// 	}
// };

namespace eagle {
class Schematic {
private:
	bool						 m_boardLoaded;
	std::string					 m_filename;
	std::vector<const Library *> m_libraries;
	std::vector<const Part *>	 m_parts;
	std::vector<const Sheet *>	 m_sheets;

private:
	void parseLibraries(rapidxml::xml_document<> &xml);
	void parseParts(rapidxml::xml_document<> &xml);
	void parseSheets(rapidxml::xml_document<> &xml);
	void parseBoardLocations(rapidxml::xml_document<> &xml);

public:
	Schematic(const std::string &filename) : m_boardLoaded(false), m_filename(filename) {
		rapidxml::xml_document<> schematic;
		rapidxml::file<>		 sch_file(filename.c_str());
		schematic.parse<0>(sch_file.data());
		parseLibraries(schematic);
		parseParts(schematic);
		parseSheets(schematic);

		std::string brdFileName(filename);
		size_t		pos = brdFileName.find_last_of(".");
		brdFileName		= brdFileName.substr(0, pos + 1) + "brd";
		try {
			rapidxml::file<>		 brd_file(brdFileName.c_str());
			rapidxml::xml_document<> board;
			board.parse<0>(brd_file.data());
			parseBoardLocations(board);
			m_boardLoaded = true;
		} catch (const std::exception &e) {
			std::cerr << e.what() << '\n';
		}
	}

	~Schematic() {
		for (const Library *lib : m_libraries) {
			delete lib;
		}
		for (const Part *part : m_parts) {
			delete part;
		}
		for (const Sheet *s : m_sheets) {
			delete s;
		}
	}

	bool								boardLoaded() const { return m_boardLoaded; }
	const std::vector<const Library *> &libraries() const { return m_libraries; }
	
	/**
	 * @brief Get reference to parts container
	 * 
	 * @return const std::vector<const Part *>& immutable reference to parts container 
	 */
	const std::vector<const Part *> &	parts() const { return m_parts; }

	void physicalParts(std::vector<const Part *> &outParts, unsigned int includeTypes = Part::Type::All) const {
		for (const Part *p : m_parts) {
			if (p->hasFootprint() && (p->type() & includeTypes)) outParts.push_back(p);
		}
	}

	const Device *	   findDevice(const std::string &fullName) const;
	const Part *	   findPart(const std::string &name) const;
	static const Part *findPart(const std::string &name, const std::vector<const Part *> &parts);
	void			   uniqueAttributes(std::vector<std::string> &attributes, bool sorted = true) const;
	void			   uniqueAttributes(std::vector<std::string> &attributes, const std::string &regex, bool sorted = true) const;

	/**
	 * @brief Groups physically equal parts.
	 *
	 * @param srcParts [in] input list which shall be grouped
	 * @param groupedParts [out] output map holding a list with the same parts (physically) for every unique part
	 * @param keys [out] output list holding the unique parts (is equal to the map's keys)
	 * @param sort [in] Sort the vectors in groupedPart.values
	 */
	static void groupPartsByValue(const std::vector<const Part *> &srcParts, std::map<const Part *, std::vector<const Part *>> &groupedParts,
								  std::vector<const Part *> &keys, bool sort = true);

	/**
	 * @brief Gathers the names of the nets that are connected on the pins of the given part
	 *
	 * @param part Part of interest
	 * @param connectedNets [out] nets that are connected to the part
	 */
	void connectedNetsForPart(const Part *part, std::vector<const Net *> &connectedNets) const;

	/**
	 * @brief filters the parts that have a pincount of at least <pincount>
	 *
	 * @param pincount
	 * @param parts [out] parts with a pincount greater or eaqual <pincount>
	 */
	void partsWithPinCount(int pincount, std::vector<const eagle::Part *> &parts) const;

	/**
	 * @brief Finds the sheet on which a gate of a part is located on
	 *
	 * @param part Part of interest
	 * @param gate Gate of interest. If nullptr the first gate of the symbol is used only
	 * @return const Sheet* Sheet on which the part/gate is located on
	 */
	const Sheet *sheetForPart(const Part *part, const Gate *gate = nullptr) const;
};
}  // namespace eagle
#endif	// EAGLE_SCHEMATIC_H_