#ifndef EAGLE_LIBRARY_H_
#define EAGLE_LIBRARY_H_

#include <list>
#include <ostream>
#include <string>

#include "device.h"
#include "deviceset.h"
#include "package.h"
#include "symbol.h"

namespace eagle {
class Library {
private:
	std::string					 m_name;
	std::list<const Package *>	 m_packages;
	std::list<const Symbol *>	 m_symbols;
	std::list<const Device *>	 m_devices;
	std::list<const Deviceset *> m_devicesets;

public:
	Library(std::string name);
	~Library();

	std::string							name() const;
	const std::list<const Package *> &	packages() const;
	const std::list<const Symbol *> &	symbols() const;
	const std::list<const Device *> &	devices() const;
	const std::list<const Deviceset *> &devicesets() const;
	void								devicesOf(const Deviceset *set, std::list<const Device *> &outDevices) const;

	void addPackage(const Package *pkg);
	void addSymbol(const Symbol *sym);
	void addDevice(const Device *dev);
	void addDeviceset(const Deviceset *set);

	const Device * findDevice(const std::string &fullName) const;
	const Package *findPackage(const std::string &name) const;
	const Symbol * findSymbol(const std::string &name) const;
};

std::ostream &operator<<(std::ostream &out, const Library &lib);
}  // namespace eagle

#endif	// EAGLE_LIBRARY_H_