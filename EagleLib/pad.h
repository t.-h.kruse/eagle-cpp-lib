#ifndef EAGLE_PAD_H_
#define EAGLE_PAD_H_
#include <string>

namespace eagle{
class Pad {
public:
	Pad(const std::string& name);

    std::string name() const;

private:
	std::string m_name;
};}

#endif	// EAGLE_PAD_H_