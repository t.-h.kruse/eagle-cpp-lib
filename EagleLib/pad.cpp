#include "pad.h"

namespace eagle {
Pad::Pad(const std::string& name) : m_name(name) {}

std::string Pad::name() const { return m_name; }
}  // namespace eagle