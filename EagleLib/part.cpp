#include "part.h"
#include <cmath>
#include <regex>

bool operator<(const eagle::Part &lhs, const eagle::Part &rhs) { return lhs.lessThan(rhs); }
bool operator>=(const eagle::Part &lhs, const eagle::Part &rhs) { return !lhs.lessThan(rhs); }

bool operator==(const eagle::Part &lhs, const eagle::Part &rhs) {
	if (lhs.type() == rhs.type()) {				 // compare type
		if (lhs.value() == rhs.value()) {		 // compare value
			if (lhs.device() == rhs.device()) {	 // compare device by their address
				// since device is the same only the part attributes
				// need to be compared
				// compare attributes

				return lhs.partAttributes().size() == rhs.partAttributes().size() &&
					   std::equal(lhs.partAttributes().begin(), lhs.partAttributes().end(), rhs.partAttributes().begin());
			}
		}
	}
	return false;
}
bool operator!=(const eagle::Part &lhs, const eagle::Part &rhs) { return !(lhs == rhs); }

namespace eagle {

// const unsigned int		Part::TypeCount = static_cast<unsigned int>(std::log2(static_cast<double>(Part::Type::UniqueTypesEnd)));
std::vector<Part::Type> Part::TypePrecedences{ Part::Type::Jumper,	   Part::Type::Resistor,	  Part::Type::Capacitor,
											   Part::Type::Inductor,   Part::Type::ValueTypesEnd, Part::Type::Diode,
											   Part::Type::Transistor, Part::Type::Relais,		  Part::Type::IC,
											   Part::Type::Oszillator, Part::Type::Connector,	  Part::Type::Switch,
											   Part::Type::Hardware,   Part::Type::Undefined,	  Part::Type::Dummy };

std::unordered_map<Part::Type, const Part::TypeAttribute> Part::TypeAttributes{ { Part::Type::Jumper, { "Jumper", "Jumper" } },
																				{ Part::Type::Resistor, { "Resistor", "Resistor" } },
																				{ Part::Type::Capacitor, { "Capacitor", "Capacitor" } },
																				{ Part::Type::Inductor, { "Inductor", "Inductor" } },
																				{ Part::Type::Diode, { "Diode", "Diode" } },
																				{ Part::Type::Transistor, { "Transistor", "Transistor" } },
																				{ Part::Type::Relais, { "Relais", "Relais" } },
																				{ Part::Type::IC, { "IC", "Integrated Circuits" } },
																				{ Part::Type::Oszillator, { "Oszillator", "Oszillator" } },
																				{ Part::Type::Connector, { "Connector", "Connector" } },
																				{ Part::Type::Switch, { "Switch", "Switch" } },
																				{ Part::Type::Hardware, { "Hardware", "Mechanical parts" } },
																				{ Part::Type::Undefined, { "Undefined", "Unknown reference designator" } },
																				{ Part::Type::Dummy, { "Dummy", "Parts without footprint" } } };

int Part::precedenceForType(const Part::Type t) {
	for (int i = 0; i < TypePrecedences.size(); i++) {
		if (TypePrecedences[i] == t) return i;
	}
	return -1;
}

std::ostream &operator<<(std::ostream &out, const Part &part) {
	out << part.name();
	if (!part.value().empty()) {
		out << "(value: " << part.value() << ")";
	} else {
		if (part.device()->deviceset()->hasUserValue()) out << "(value: -)";
	}
	const std::map<std::string, std::string> &attributes = part.partAttributes();
	if (!attributes.empty()) {
		out << ", attributes: [";

		auto											   secondLast = std::prev(attributes.end(), 1);
		std::map<std::string, std::string>::const_iterator it;
		for (it = attributes.begin(); it != secondLast; it++) {
			out << it->first << ": " << it->second << ", ";
		}

		out << it->first << ": " << it->second << "]";
	}
	out << "[" << *part.device() << "]";
	// out << ".s" << *part.sheet();
	return out;
}

Part::Part(const std::string &name, const std::string &value, const Device *dev)
	: m_name(name)
	, m_refDesIndex(-1)
	, m_refDesPrefix()
	, m_value(value)
	, m_valueStandardized(-1)
	, m_device(dev)
	// , m_sheet(nullptr)
	, m_inferredType(Type::Undefined)
	, m_attributes()
	, m_boardLocation() {
	if (dev) {
		if (const Package *pck = dev->package()) {
			const std::string &refdes = dev->deviceset()->prefix();
			if (refdes == "C") {
				m_inferredType = Type::Capacitor;
				updateStandardizedValue();
			} else if (refdes == "D") {
				m_inferredType = Type::Diode;
			} else if (refdes == "J" || refdes == "JP" || refdes == "JMP") {
				m_inferredType = Type::Jumper;
				updateStandardizedValue();
			} else if (refdes == "L") {
				m_inferredType = Type::Inductor;
				updateStandardizedValue();
			} else if (refdes == "H")
				m_inferredType = Type::Hardware;
			else if (refdes == "K")
				m_inferredType = Type::Relais;
			else if (refdes == "Q")
				m_inferredType = Type::Transistor;
			else if (refdes == "R") {
				m_inferredType = Type::Resistor;
				updateStandardizedValue();
			} else if (refdes == "S")
				m_inferredType = Type::Switch;
			else if (refdes == "U" || refdes == "N")
				m_inferredType = Type::IC;
			else if (refdes == "X" || refdes == "ST")
				m_inferredType = Type::Connector;
			else if (refdes == "Y")
				m_inferredType = Type::Oszillator;
		} else
			m_inferredType = Type::Dummy;
	}
	updateReferenceDesignator();
}

bool Part::lessThan(const Part &rhs, Sort mode) const {
	int lPrio = Part::precedenceForType(type());
	int rPrio = Part::precedenceForType(rhs.type());

	if (lPrio == rPrio) {  // sort by type
		switch (mode) {
			case Sort::ByValue: {
				const Package *lpck = device()->package();
				const Package *rpck = rhs.device()->package();
				if (lpck && rpck) {	 // check that both parts are no dummy type
					if (!lpck || !rpck) throw std::runtime_error(std::string(__FUNCTION__) + " packages must not be null!");
					if (lpck->name() == rpck->name()) {						   // sort by package
						if (valueStandardized() == rhs.valueStandardized()) {  // sort by value (standardized)
							if (value() == rhs.value()) {					   // sort by value (string)
								std::string lPartPrefix = referenceDesignatorPrefix();
								std::string rPartPrefix = rhs.referenceDesignatorPrefix();
								if (lPartPrefix == rPartPrefix) {  // sort by name prefix
									int lPartIndex = referenceDesignatorIndex();
									int rPartIndex = rhs.referenceDesignatorIndex();
									// std::cout << "Sort by name index" << std::endl;
									return lPartIndex < rPartIndex;	 // sort by name index
								} else {
									// std::cout << "Sort by name prefix" << std::endl;
									return lPartPrefix < rPartPrefix;
								}
							} else {
								return value() < rhs.value();
							}
						} else {
							// std::cout << "Sort by value" << std::endl;
							return valueStandardized() < rhs.valueStandardized();
						}
					} else {
						// std::cout << "Sort by package" << std::endl;
						return device()->package()->name() < rhs.device()->package()->name();
					}
				} else {  // sort dummy types by their name
					std::string lPartPrefix = referenceDesignatorPrefix();
					std::string rPartPrefix = rhs.referenceDesignatorPrefix();
					if (lPartPrefix == rPartPrefix) {  // sort by name prefix
						int lPartIndex = referenceDesignatorIndex();
						int rPartIndex = rhs.referenceDesignatorIndex();
						// s<td::cout << "Sort by name index" << std::endl;
						return lPartIndex < rPartIndex;	 // sort by name index
					} else {
						// std::cout << "Sort by name prefix" << std::endl;
						return lPartPrefix < rPartPrefix;
					}
				}
			}
			case Sort::ByName: {
				std::string lPartPrefix = referenceDesignatorPrefix();
				std::string rPartPrefix = rhs.referenceDesignatorPrefix();
				if (lPartPrefix == rPartPrefix) {  // sort by name prefix
					int lPartIndex = referenceDesignatorIndex();
					int rPartIndex = rhs.referenceDesignatorIndex();
					// s<td::cout << "Sort by name index" << std::endl;
					return lPartIndex < rPartIndex;	 // sort by name index
				} else {
					// std::cout << "Sort by name prefix" << std::endl;
					return lPartPrefix < rPartPrefix;
				}
			}
			default: throw std::runtime_error(std::string(__FUNCTION__) + (" default case should never be reached!")); break;
		}
	} else {
		// std::cout << "Sort by prio" << std::endl;
		return lPrio < rPrio;
	}
}

std::string Part::name() const { return m_name; }
std::string Part::value(bool inheritFromDevice) const {
	if ((inheritFromDevice && m_device->deviceset()->hasUserValue()) || !m_value.empty()) {
		return m_value;
	} else {
		return m_device->name();
	}
}

void Part::updateStandardizedValue() {
	if (!m_value.empty()) {
		double		result = -1;
		std::string str	   = m_value;
		std::transform(str.begin(), str.end(), str.begin(), ::tolower);
		std::smatch m;
		std::regex	re("(\\d+meg\\d*)|(\\d+[.fpnuµmrkg]\\d*)|\\d+");
		if (std::regex_search(str, m, re)) {
			double		exp = 0;
			std::string val = m[0].str();
			str				= m.suffix().str();

			std::size_t		  found = val.find_first_not_of("0123456789");
			const std::string meg	= "meg";
			if (found != std::string::npos) {
				switch (val[found]) {
					case 'f': exp = -15; break;
					case 'p': exp = -12; break;
					case 'n': exp = -9; break;
					case 'u': exp = -6; break;
					// case 'µ': exp = -6; break;
					case 'm': {
						if (val.compare(found, meg.size(), meg) == 0) {
							exp = 6;
							val = val.substr(0, found + 1) + val.substr(found + meg.size(), std::string::npos);
						} else {
							exp = -3;
						}
						break;
					}
					case 'k': exp = 3; break;
					case 'g': exp = 9; break;
					default: break;
				}
				val[found] = '.';
			}
			if (!str.empty()) {
				switch (str[0]) {
					case 'f': exp += -15; break;
					case 'p': exp += -12; break;
					case 'n': exp += -9; break;
					case 'u': exp += -6; break;
					// case 'µ': exp += -6; break;
					case 'm': {
						if (str.compare(0, meg.size(), meg) == 0) {
							exp += 6;
						} else {
							exp += -3;
						}
						break;
					}
					case 'k': exp += 3; break;
					case 'g': exp += 9; break;
					default: break;
				}
			}
			result = std::strtod(val.c_str(), nullptr);
			exp	   = std::pow(10, exp);
			result *= exp;
		}
		m_valueStandardized = result;
	}
}

void Part::setValue(const std::string &newValue) {
	m_value = newValue;
	updateStandardizedValue();
}

double Part::valueStandardized() const { return m_valueStandardized; }

void Part::updateReferenceDesignator() {
	std::string str	  = m_name;
	std::size_t found = str.find_first_of("0123456789");
	if (found != std::string::npos) {
		m_refDesPrefix = str.substr(0, found);
		str			   = str.substr(found, std::string::npos);
		m_refDesIndex  = std::strtoul(str.c_str(), nullptr, 10);
	} else {
		m_refDesPrefix = str;
	}
}

std::vector<const Part *>::const_iterator Part::findIn(const std::vector<const Part *> &list) const {
	std::vector<const Part *>::const_iterator it;
	for (it = list.begin(); it != list.end(); it++) {
		const Part &p = **it;
		if (*this == p) break;
		;
	}
	return it;
}

int Part::referenceDesignatorIndex() const { return m_refDesIndex; }

const std::string &Part::referenceDesignatorPrefix() const { return m_refDesPrefix; }

const Device *Part::device() const { return m_device; }
bool		  Part::hasFootprint() const { return (m_device->package()) ? true : false; }
bool		  Part::hasDatasheet() const {
	 if (m_device)
		 return m_device->deviceset()->hasDatasheet();
	 else
		 return false;
}
std::string Part::datasheet() const { return m_device->deviceset()->datasheet(); }
// const Sheet *Part::sheet() const { return m_sheet; }
// void Part::setSheet(const Sheet *newValue) { m_sheet = newValue; }

Part::Type Part::type() const { return m_inferredType; }

bool Part::isValueType() const { return m_inferredType < Type::ValueTypesEnd; }

std::string Part::attribute(const std::string &key, bool inherit) const {
	if (!inherit) {
		try {
			return m_attributes.at(key);
		} catch (const std::out_of_range &) {
			throw;
		}
	} else {
		try {
			return m_attributes.at(key);
		} catch (const std::out_of_range &) {
			try {
				return m_device->attribute(key);
			} catch (const std::out_of_range &) {
				throw;
			}
		}
	}
}

void Part::setAttribute(const std::string &key, const std::string &value) {
	if (m_device->hasAttribute(key)) {	// check if the device defines this attribute
		const std::string dev_attr = m_device->attribute(key);
		if (dev_attr != value) {
			m_attributes[key] = value;	// add the attribute to the part if it differs from the device attribute
		} else {
			if (hasAttribute(key, false)) {
				// delete the attribute from the part if it would be the same as for the device
				m_attributes.erase(key);
			}
		}
	} else {
		if (value.empty())
			m_attributes.erase(key);
		else
			m_attributes[key] = value;	// add or update the part's attribute
	}
}

const std::map<std::string, std::string> &Part::partAttributes() const { return m_attributes; }
const std::map<std::string, std::string> &Part::deviceAttributes() const { return m_device->attributes(); }

void Part::attributes(std::map<std::string, std::string> &attributes) const {
	attributes = m_device->attributes();
	std::map<std::string, std::string>::const_iterator it;
	for (it = m_attributes.begin(); it != m_attributes.end(); it++) {
		attributes[it->first] = it->second;
	}
}

bool Part::hasAttribute(const std::string &key, bool inheritFromDevice) const {
	return (this->m_attributes.find(key) != this->m_attributes.end()) || (inheritFromDevice && m_device->hasAttribute(key));
}
void Part::deleteAttribute(const std::string &key) { m_attributes.erase(key); }

void Part::setAttributes(const std::map<std::string, std::string> &attributes) { this->m_attributes = attributes; }

const BoardLocation &Part::boardLocation() const { return m_boardLocation; }
void				 Part::setBoardLocation(double x, double y, double rotation, bool bottom) { m_boardLocation = BoardLocation(x, y, rotation, bottom); }
void				 Part::setBoardLocation(const BoardLocation &loc) { m_boardLocation = loc; }

}  // namespace eagle