#include "sheet.h"

namespace eagle {
std::ostream &operator<<(std::ostream &out, const Sheet &sheet) {
	out << sheet.number();
	return out;
}

Sheet::Sheet(unsigned int number, const std::string &desc) : m_number(number), m_description(desc) {}
Sheet::~Sheet() {
	for (const Net *n : m_nets) {
		delete n;
	}
}

unsigned int Sheet::number() const { return m_number; }
std::string	 Sheet::description() const { return m_description; }

void Sheet::setDescription(const std::string &newValue) { m_description = newValue; }

const std::vector<const Net *> Sheet::nets() const { return m_nets; }
void						   Sheet::nets(const Part *part, const Gate *gate, std::vector<const Net *> &nets) const {
	  for (const Net *n : m_nets) {
		  if (n->connects(part, gate)) nets.push_back(n);
	  }
}
void Sheet::addNet(const Net *net) { m_nets.push_back(net); }

void Sheet::addInstance(const Part *part, const Gate *gate) { m_instances.push_back(PartInstance(part, gate)); }
void Sheet::addInstance(const PartInstance &instance) { m_instances.push_back(instance); }
bool Sheet::hasPart(const Part *part) const {
	for (const PartInstance &inst : m_instances) {
		if (inst.part() == part) return true;
	}
	return false;
}
bool Sheet::hasGate(const Part *part, const Gate *gate) const {
	for (const PartInstance &inst : m_instances) {
		if (inst.part() == part && inst.gate() == gate) return true;
	}
	return false;
}
}  // namespace eagle