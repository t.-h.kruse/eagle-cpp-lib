#ifndef EAGLE_PIN_H_
#define EAGLE_PIN_H_

#include <ostream>
#include <string>
namespace eagle {
class Pin {
public:
	enum Direction { NC, IN, OUT, IO, OC, HIZ, PAS, PWR, SUP };

	Pin(const std::string &name, Direction dir);
	std::string name() const;
	Direction	direction() const;

private:
	Direction	m_direction;
	std::string m_name;
};

std::ostream &operator<<(std::ostream &out, const Pin &pin);
}  // namespace eagle
#endif	// EAGLE_PIN_H_