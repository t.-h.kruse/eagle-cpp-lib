#ifndef EAGLE_BOARDLOCATION_H_
#define EAGLE_BOARDLOCATION_H_

namespace eagle {

class BoardLocation {
private:
	double m_x;
	double m_y;
	double m_rotation;
	bool   m_bottom;

public:
	BoardLocation();
	BoardLocation(double x, double y, double rotation, bool bottom);

	double x() const;
	double y() const;
	double rotation() const;
	bool   bottom() const;
	void   setX(double newValue);
	void   setY(double newValue);
	void   setRotation(double newValue);
	void   setBottom(bool newValue);
};

}  // namespace eagle

#endif	// EAGLE_BOARDLOCATION_H_