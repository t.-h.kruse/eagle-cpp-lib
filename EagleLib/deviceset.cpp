#include <map>
#include <regex>
#include <string>

#include "deviceset.h"

namespace eagle {
std::ostream& operator<<(std::ostream& out, const Deviceset& set) {
	out << set.name();
	return out;
}

Deviceset::Deviceset(const std::string& name, const std::string& prefix, const std::string& description, bool hasUserPrefix)
	: m_name(name), m_prefix(prefix), m_description(), m_datasheet(), m_hasUserValue(hasUserPrefix) {
	static const std::map<std::string, std::string> html_replacements{ { "&lt;", "<" }, { "&gt;", ">" } };

	std::string str(description);

	std::map<std::string, std::string>::const_iterator it;
	for (it = html_replacements.begin(); it != html_replacements.end(); it++) {
		std::regex re(it->first, std::regex_constants::icase);
		str = std::regex_replace(str, re, it->second);
	}
	m_description = str;

	std::size_t pos = str.find("href=\"");
	if (pos != std::string::npos) {
		std::size_t start_pos = pos + 6;
		std::size_t end_pos	  = str.find("\"", start_pos);
		if (end_pos != std::string::npos) {
			m_datasheet = str.substr(start_pos, end_pos - start_pos);
		}
	}
}

std::string Deviceset::name() const { return m_name; }
std::string Deviceset::prefix() const { return m_prefix; }
std::string Deviceset::description() const { return m_description; }
std::string Deviceset::datasheet() const { return m_datasheet; }
bool		Deviceset::hasDatasheet() const { return !m_datasheet.empty(); }
bool		Deviceset::hasUserValue() const { return m_hasUserValue; }

void		Deviceset::addGate(const Gate* gate) { m_gates.push_back(gate); }
const Gate* Deviceset::gate(const std::string& gateName) const {
	for (const Gate* g : m_gates) {
		if (g->name() == gateName) return g;
	}
	return nullptr;
}

const std::vector<const Gate*>& Deviceset::gates() const { return m_gates; }

void Deviceset::assign(const Pad* pad, const Pin* pin) { m_pinAssignment[pad] = pin; }
}  // namespace eagle