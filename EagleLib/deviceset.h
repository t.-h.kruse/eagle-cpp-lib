#ifndef EAGLE_DEVICESET_H_
#define EAGLE_DEVICESET_H_

#include <map>
#include <ostream>
#include <string>
#include <vector>

#include "gate.h"
#include "pad.h"

namespace eagle {

class Deviceset {
private:
	std::string						 m_name;
	std::string						 m_prefix;
	std::string						 m_description;
	std::string						 m_datasheet;
	bool							 m_hasUserValue;
	std::vector<const Gate*>		 m_gates;
	std::map<const Pad*, const Pin*> m_pinAssignment;

public:
	Deviceset(const std::string& name, const std::string& prefix, const std::string& description, bool hasUserValue = false);

	std::string name() const;
	std::string prefix() const;
	std::string description() const;
	std::string datasheet() const;
	bool		hasDatasheet() const;
	bool		hasUserValue() const;

	void							addGate(const Gate* gate);
	const Gate*						gate(const std::string& gateName) const;
	const std::vector<const Gate*>& gates() const;
	void							assign(const Pad* pad, const Pin* pin);
};

std::ostream& operator<<(std::ostream& out, const Deviceset& set);
}  // namespace eagle

#endif	// EAGLE_DEVICESET_H_