#include "pin.h"

namespace eagle {
std::ostream &operator<<(std::ostream &out, const Pin &pin) {
	out << "Pin(" << pin.name() << ", ";
	switch (pin.direction()) {
		case Pin::Direction::NC: out << "nc"; break;
		case Pin::Direction::IN: out << "in"; break;
		case Pin::Direction::OUT: out << "out"; break;
		case Pin::Direction::IO: out << "io"; break;
		case Pin::Direction::OC: out << "pc"; break;
		case Pin::Direction::HIZ: out << "hiz"; break;
		case Pin::Direction::PAS: out << "pas"; break;
		case Pin::Direction::PWR: out << "pwr"; break;
		case Pin::Direction::SUP: out << "sup"; break;
		default: break;
	}
    out << ")";
	return out;
}

Pin::Pin(const std::string &name, Direction dir) : m_name(name), m_direction(dir) {}

std::string	   Pin::name() const { return m_name; }
Pin::Direction Pin::direction() const { return m_direction; }

}  // namespace eagle