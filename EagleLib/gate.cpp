#include "gate.h"
namespace eagle {
Gate::Gate(const std::string& name, const Symbol* symbol, int swaplevel)
	: m_symbol(symbol), m_name(name), m_swaplevel(swaplevel) {}

std::string	  Gate::name() const { return m_name; }
const Symbol* Gate::symbol() const { return m_symbol; }
int			  Gate::swaplevel() const { return m_swaplevel; }
}  // namespace eagle