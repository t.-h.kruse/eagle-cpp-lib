#ifndef EAGLE_PACKAGE_H_
#define EAGLE_PACKAGE_H_

#include <string>
#include <vector>

#include <ostream>

#include "pad.h"

namespace eagle {
class Package {
public:
	Package(std::string name);
	~Package();

	std::string					   name() const;
	const std::vector<const Pad *> pads() const;
	std::size_t					   padCount() const;

	void	   addPad(const Pad *pad);
	const Pad *pad(const std::string& pad) const;

private:
	std::string				 m_name;
	std::vector<const Pad *> m_pads;
};

std::ostream &operator<<(std::ostream &out, const Package &pkg);
}  // namespace eagle

#endif	// EAGLE_PACKAGE_H_