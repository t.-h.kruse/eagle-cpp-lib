#ifndef EAGLE_GATE_H_
#define EAGLE_GATE_H_

#include <string>
#include "symbol.h"

namespace eagle {
class Gate {
public:
	Gate(const std::string &name, const Symbol *symbol, int swaplevel);

	std::string	  name() const;
	const Symbol *symbol() const;
	int			  swaplevel() const;

private:
	const Symbol *m_symbol;
	std::string	  m_name;
	int			  m_swaplevel;
};
}  // namespace eagle

#endif	// EAGLE_GATE_H_