#ifndef EAGLE_NET_H_
#define EAGLE_NET_H_

#include <string>
#include <vector>

namespace eagle {

class Part;
class Pin;
class Gate;

typedef struct NetConnection_s {
	const Part* part;
	const Gate* gate;
	const Pin*	pin;
} NetConnection;

class Net {
public:
	Net(const std::string& name);

	std::string name() const;
	void		addConnection(const NetConnection& con);

	bool connects(const Part* p) const;
	const Pin* connects(const Part* p, const Gate *g) const;
	bool connects(const Part* part, const Gate *gate, const Pin* pin) const;

private:
	std::string				   m_name;
	std::vector<NetConnection> m_connectedParts;
};
}  // namespace eagle

#endif	// EAGLE_NET_H_