#ifndef EAGLE_PART_H_
#define EAGLE_PART_H_

#include <iostream>
#include <map>
#include <unordered_map>
#include <string>
#include <vector>

#include "boardlocation.h"
#include "device.h"
#include "sheet.h"

namespace eagle {
class Part {
public:
	static const unsigned int NumTypes = 14;
	// https://en.wikipedia.org/wiki/Reference_designator
	// IEEE 200-1975/ANSI Y32.16-1975
	enum Type {
		// Here come unique Types
		Jumper		   = (1 << 0),
		Resistor	   = (1 << 1),
		Capacitor	   = (1 << 2),
		Inductor	   = (1 << 3),
		ValueTypesEnd  = (1 << 3),
		Diode		   = (1 << 4),
		Transistor	   = (1 << 5),
		Relais		   = (1 << 6),
		IC			   = (1 << 7),
		Oszillator	   = (1 << 8),
		Connector	   = (1 << 9),
		Switch		   = (1 << 10),
		Hardware	   = (1 << 11),
		Undefined	   = (1 << 12),
		Dummy		   = (1 << 13), /**< Parts without package like supply symbols*/
		UniqueTypesEnd = (1 << (NumTypes - 1)),
		// Here come combinations/groups
		All = (1 << (NumTypes)) - 1
	};

	struct TypeAttribute {
		const std::string name;
		const std::string description;
	};
	static std::unordered_map<Type, const TypeAttribute> TypeAttributes;

	enum Sort { ByValue, ByName };

	// static const unsigned int TypeCount;
	static std::vector<Type>  TypePrecedences;
	static int				  precedenceForType(const Type t);

private:
	std::string						   m_name;				/**< Part designator/name*/
	int								   m_refDesIndex;		/**< Number of the partname. Eg: R122 has an index of 122. Invalid if < 0*/
	std::string						   m_refDesPrefix;		/**< Prefix of the partname: Eg: RK1 has the prefix "RK"*/
	std::string						   m_value;				/**< User set value*/
	double							   m_valueStandardized; /**< Value without unit modifiers (kilo, mega, etc), used for sorting by value. Invalid if < 0*/
	const Device *					   m_device;			/**< Reference to the device/library*/
	Type							   m_inferredType;		/**< Inferred type based on device*/
	std::map<std::string, std::string> m_attributes;		/**< Attributes set in the schematic*/
	BoardLocation					   m_boardLocation;

	void updateReferenceDesignator();

public:
	Part(const std::string &name, const std::string &value, const Device *dev);

	std::string		   name() const;
	std::string		   value(bool inheritFromDevice = true) const;
	double			   valueStandardized() const;
	void			   setValue(const std::string &newValue);
	void			   updateStandardizedValue();
	int				   referenceDesignatorIndex() const;
	const std::string &referenceDesignatorPrefix() const;
	const Device *	   device() const;
	bool			   hasFootprint() const;
	bool			   hasDatasheet() const;
	std::string		   datasheet() const;
	Type		type() const;
	bool		isValueType() const;
	std::string attribute(const std::string &key, bool inherit = true) const;

	/**
	 * @brief Sets the [key] attribute of the part
	 * If [value] is already defined by the device
	 * the [key] from the part is removed
	 *
	 * @param key
	 * @param value
	 */
	void setAttribute(const std::string &key, const std::string &value);
	bool hasAttribute(const std::string &key, bool inheritFromDevice = true) const;
	/**
	 * @brief Gathers all attributes for the part taking into account the inherited attributes
	 *
	 * @param attributes [out] Gathered attributes
	 */
	void									  attributes(std::map<std::string, std::string> &attributes) const;
	const std::map<std::string, std::string> &partAttributes() const;
	const std::map<std::string, std::string> &deviceAttributes() const;

	const BoardLocation &boardLocation() const;
	void				 setBoardLocation(double x, double y, double rotation, bool bottom);
	void				 setBoardLocation(const BoardLocation &loc);

	std::vector<const Part *>::const_iterator findIn(const std::vector<const Part *> &list) const;

	void deleteAttribute(const std::string &key);
	void setAttributes(const std::map<std::string, std::string> &attributes);

	/**
	 * @brief Compares two parts by "less than"
	 * The parts are compared according this precedence:
	 * 1. Type
	 * 2. Package
	 * 3. Value (standardized)
	 * 4. Value (string)
	 * 5. Name prefix
	 * 6. Name index
	 * @param rhs
	 * @param mode
	 */
	bool lessThan(const Part &rhs, Sort mode = Sort::ByName) const;
};

std::ostream &operator<<(std::ostream &out, const Part &part);
}  // namespace eagle

bool operator<(const eagle::Part &lhs, const eagle::Part &rhs);
bool operator>=(const eagle::Part &lhs, const eagle::Part &rhs);
bool operator==(const eagle::Part &lhs, const eagle::Part &rhs);
bool operator!=(const eagle::Part &lhs, const eagle::Part &rhs);

#endif	// EAGLE_PART_H_