#include <iostream>
#include <string>
#include <vector>

#include "EagleLib/commands/CommandHistory.h"
#include "EagleLib/schematic.h"

std::map<const eagle::Part*, std::vector<const eagle::Part*>> m_groupedParts;
std::vector<const eagle::Part*>								  m_groupedPartsKeys;

const eagle::Part* keyForPart(const eagle::Part* part) {
	for (auto [k, v] : m_groupedParts) {
		if (std::find(v.begin(), v.end(), part) != v.end()) return k;
	}
	return nullptr;
}

void mergeKeys(const std::vector<const eagle::Part*> keys) {
	std::vector<const eagle::Part*>& v0 = m_groupedParts.at(keys.front());
	auto							 it = keys.begin() + 1;
	for (; it != keys.end(); it++) {
		auto vn = m_groupedParts.at(*it);
		v0.insert(v0.end(), std::make_move_iterator(vn.begin()), std::make_move_iterator(vn.end()));  // move elements from vn to v0
		m_groupedParts.erase(*it);																	  // remove key-value pair

		// find key also in groupedKeys vector
		auto keysVectorIt = std::find(m_groupedPartsKeys.begin(), m_groupedPartsKeys.end(), *it);
		if (keysVectorIt != m_groupedPartsKeys.end()) {
			m_groupedPartsKeys.erase(keysVectorIt);
		}
	}
}

std::vector<const eagle::Part*> partsChanged(const std::vector<const eagle::Part*>& parts, bool partsAreKeys) {
	std::vector<const eagle::Part*> removedKeys;
	for (auto part : parts) {
		if (partsAreKeys) {
			// cases to consider
			// - keys may now be duplicates -> merge

			// check if key does match any existing key
			std::vector<const eagle::Part*> equalKeys;
			for (const eagle::Part* key : m_groupedPartsKeys) {
				if (*key == *part) {
					equalKeys.push_back(key);
				}
			}

			if (equalKeys.size() > 1) {
				mergeKeys(equalKeys);
				removedKeys.insert(removedKeys.end(), equalKeys.begin() + 1, equalKeys.end());
			}
		} else {
			if (auto previousKey = keyForPart(part)) {
				// cases to consider
				//  - when part is also the key and vector is not empty after removal
				//  - when part is also the key and part is only element in vector
				//  - keys have to be updated in groupedPartsKeys as well

				std::vector<const eagle::Part*>& values = m_groupedParts.at(previousKey);

				if (values.size() > 1) {		// vector will not be empty after removal
					if (part == previousKey) {	// part is also the key
						// move other parts in new map-bucket
						// if (part == previousKey) then part is always first element in vector
						std::vector<const eagle::Part*> newValues;
						newValues.insert(newValues.end(), std::make_move_iterator(values.begin() + 1), std::make_move_iterator(values.end()));
						values.erase(values.begin() + 1, values.end());

						// check if the parts in the new vector match any existing key
						std::vector<const eagle::Part*> equalKeys;
						for (const eagle::Part* key : m_groupedPartsKeys) {
							if (*key == *newValues.front()) {
								equalKeys.push_back(key);
							}
						}

						if (equalKeys.empty()) {  // newValues will be in a new key
							m_groupedParts[newValues.front()] = newValues;
							m_groupedPartsKeys.push_back(newValues.front());
						} else {
							// newValues will be inserted into equalKeys.front().values
							std::vector<const eagle::Part*>& equalKeysFrontValues = m_groupedParts.at(equalKeys.front());
							std::copy(newValues.begin(), newValues.end(), std::back_inserter((equalKeysFrontValues)));
							// if equalKeys.size() > 1 -> merge remaining equalKeys with first equalKey
							if (equalKeys.size() > 1) {
								mergeKeys(equalKeys);
								removedKeys.insert(removedKeys.end(), equalKeys.begin() + 1, equalKeys.end());
							}
						}
					} else {
						if (*part != *previousKey) {  // only perform changes if the part does not fit into current key
							// remove part from old vector
							auto it = std::find(values.begin(), values.end(), part);
							if (it != values.end()) {
								values.erase(it);
							}

							// check if the part matches any existing key
							std::vector<const eagle::Part*> equalKeys;
							for (const eagle::Part* key : m_groupedPartsKeys) {
								if (*key == *part) {
									equalKeys.push_back(key);
								}
							}

							// place part in new map-bucket
							if (!equalKeys.empty()) {  // newKey exists
								// insert part in new vector
								m_groupedParts.at(equalKeys.front()).push_back(part);
								// if equalKeys.size() > 1 -> merge remaining equalKeys with first equalKey

								if (equalKeys.size() > 1) {
									mergeKeys(equalKeys);
									removedKeys.insert(removedKeys.end(), equalKeys.begin() + 1, equalKeys.end());
								}

							} else {  // newKey does not exist
									  // key has to be created
								m_groupedParts[part] = { part };
								m_groupedPartsKeys.push_back(part);
							}
						}
					}
				} else {  // else (values.size == 1) no change on map entry since key and part are equal and a change does not affect this container
					// check if the parts in the new vector match any existing key
					std::vector<const eagle::Part*> equalKeys;
					for (const eagle::Part* key : m_groupedPartsKeys) {
						if (*key == *part) {
							equalKeys.push_back(key);
						}
					}
					if (equalKeys.size() > 1) {
						mergeKeys(equalKeys);
						removedKeys.insert(removedKeys.end(), equalKeys.begin() + 1, equalKeys.end());
					}
				}
			} else {
				assert(true);  // reaching here means that the part is not sorted into groupedParts -> should never be the case
			}
		}
	}
	return removedKeys;
}

int main(int argc, char const* argv[]) {
	using namespace std;
	using namespace eagle;
	// Schematic sch("D:\\work\\projects\\ledbgm\\ecad\\ledbgm.sch");
	Schematic sch("D:\\work\\projects\\aeg-dac-card\\hw\\ecad\\FG650_340_AEG_DAC_Modul.sch");
	// Schematic	   sch("E:/Documents/projects/aeg-dac-card/hw/ecad/FG650_340_AEG_DAC_Modul.sch");
	CommandHistory commands;

	Schematic::groupPartsByValue(sch.parts(), m_groupedParts, m_groupedPartsKeys);

	// bug: after undo the key for R22 appears as its own key with 3 elements
	if (Part* part = const_cast<Part*>(sch.findPart("R1"))) {
		if (const Part* key = keyForPart(part)) {
			// commands.push(new AddAttributeCommand(m_groupedParts.at(key), "MPN", "GPR0603100R"));
			commands.push(new ChangeValueCommand({ part }, "11R"));
			// partsChanged({ key }, true);
			partsChanged({ part }, false);
			if (auto cmd = commands.undo()) {
				partsChanged(cmd->parts(), false);
			}
		}
	}

	return 0;
}