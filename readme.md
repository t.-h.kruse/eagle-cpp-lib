# EAGLE Cpp Lib

This Library is intended to provide an interface to parse data from EAGLE schematic files (*.sch).

# Usage

Building:

    $ mkdir build\Debug && cd build\Debug
    $ cmake -G Ninja -DCMAKE_INSTALL_PREFIX=D:/programming/myroot ..

or via presets in the project's top level directory:

    cmake . --preset mylab-debug

Installing:

    $ cmake --build build/mylab-Debug --target install